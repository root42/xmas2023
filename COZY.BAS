option binary base 0
defint A-Z

'--------------------------------------------
' Init VGA Mode 13, 320x200 256 colors
'--------------------------------------------
sub initgfx
  reg 1, &h0013
  call interrupt &h10
end sub

'--------------------------------------------
' Init mode 03h, text mode
'--------------------------------------------
sub inittext
  reg 1, &h0003
  call interrupt &h10
end sub

'--------------------------------------------
' Vertical blank sync
'--------------------------------------------
sub vsync
vsync1:
  v=inp(&h3da)
  if v and 8 <> 0 then goto vsync1
vsync0:
  v=inp(&h3da)
  if v and 8 = 0 then goto vsync0
end sub

'--------------------------------------------
' Load 320x200 PCX file
'--------------------------------------------
sub loadpcx( filename$ )
  open filename$ for binary as #1
  get$ #1, 128, header$
  def seg = strseg(header$)
  header& = strptr(header$)
  a = peek(header&)
  if a<>&h0a then
    inittext : print "NOT A PCX FILE", a : END
  end if
  incr header&
  incr header&
  a = peek(header&)
  if a<>1 then
    inittext : print "NOT RLE ENCODED", a : END
  end if
  def seg

  dim pal_ as local string
  seek 1, lof(1)-768
  get$ #1, 768, pal_
  def seg = strseg(pal_)
  pal = strptr(pal_)
  out &h3c8, 0
  for i = 0 to 767
    col = peek(pal+i)
    shift right col, 2
    out &h3c9, col
  next i

  dim pix_ as local string * 1
  count& = 0
  seek 1, 128
  while count& < 64000
    get$ #1, 1, pix_
    pix = asc(pix_)
    if pix >= 192 then
      numpix = pix - 192
      get$ #1, 1, pix_
      pix = asc(pix_)
      def seg = &ha000
      while numpix > 0
        poke count&, pix
        incr count&
        decr numpix
      wend
      def seg
    else
      def seg = &ha000
      poke count&, pix
      incr count&
      def seg
    end if
  wend
  close #1
end sub

'--------------------------------------------
' Initialize Fire
'--------------------------------------------
sub init_fire
  def seg = &ha000
  for y = 103 to 133
    for x = 140 to 195
      poke x+y*320, 128
    next x
  next y
  def seg
end sub

'--------------------------------------------
' Update Fire
'--------------------------------------------
sub update_fire
  def seg = &ha000
    for i = 0 to 54
      if i < 12 or i > 42 then
        r1 = 128 : r2 = 128
      else
        r1 = 128+rnd*127
        r2 = 128+rnd*127
      end if
      poke 141+132*320+i, r1
      poke 141+133*320+i, r2
    next i
    for y = 104 to 131
      for x = 143 to 190
        r1 = -1 + rnd*2
        r2 = -1 + rnd*2
        col =       peek(x+r1-1+(y+r2+1)*320)-128
        col = col + peek(x+r1-0+(y+r2+1)*320)-128
        col = col + peek(x+r1+1+(y+r2+1)*320)-128
        col = col / 3.4
        poke x+y*320,col+128
      next x
    next y
  def seg
end sub

'--------------------------------------------
' MAIN PROGRAM
'--------------------------------------------
cls
randomize timer
initgfx
loadpcx( "CHIMNEY.PCX" )
init_fire
do
  vsync
  update_fire
loop until instat
inittext